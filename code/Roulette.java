import java.util.Scanner;

public class Roulette {
    public static void main (String[] args) {
        Scanner kb = new Scanner(System.in);

        //Initializing
        RouletteWheel roulette = new RouletteWheel();
        int playerMoney = 1000; //start money 
        int betNumber;          //number player choose
        int betMoney;           //amount of money player bet
        boolean quit = false;   //for the loop
        String input = "";      //also for the loop

        //Asking for inputs
        System.out.println("You start with 1000$");

       // while(!quit) { Was for bonus question
            System.out.println("How much would you like to bet?");
            betMoney = kb.nextInt();
            //Verifying amount
            while(!(betMoney <= playerMoney || betMoney <=0)) {
                System.out.println("Please choose a valid amount");
                System.out.println("You have: " + playerMoney + "$");
                betMoney = kb.nextInt();
            }
            System.out.println("Which number would you like to bet on?");
            betNumber = kb.nextInt();

            roulette.spin();
            System.out.println("the number was: "+ roulette.getValue());
            if(betNumber == roulette.getValue()) {
                System.out.println("You won " + (betMoney * (35)) + "!!!");
            } else {  
                System.out.println("You sadly lost");
                playerMoney -= betMoney;
            }
            System.out.println("you now have: " + playerMoney + "$");
/*
            //Quit or no (bonus, just me trying to remember java)
            
            System.out.println("you now have: " + playerMoney);
            System.out.println("Would you like to continue?");
            input = kb.nextLine();
            input = input.toUpperCase();
            
            while(!(input.equals("NO") || input.equals("N") || input.equals("YES") || input.equals("Y") )) {
                System.out.println("Please input a valid input");
                input = kb.nextLine();
                input = input.toUpperCase();
            }
            
            if(input.equals("NO") || input.equals("N")) {
                System.out.println("ok bye bye");
                quit = true;
                } else {
                    if(playerMoney == 0) {
                    playerMoney = 1000;
                    System.out.println("You have 0$, I give 1000$, you are in debt");
                    input = "";
                }
            } 
        } */
    }
}