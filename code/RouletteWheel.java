import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int num;

    public RouletteWheel() {
        random = new Random();
        this.num = 0;
    }

    public void spin() {
        this.num = random.nextInt(38); 
    }

    public int getValue() {
        return this.num;
    }



}
